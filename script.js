
let allTabs = document.querySelectorAll("[data-target]");
let allTabContent = document.querySelectorAll("[data-tab-content]");

allTabs.forEach(e => {
    e.addEventListener('click',() => {
        allTabs.forEach(el => {
            el.classList.remove('active');
        });

        let activeTab = document.getElementById(e.dataset.target);
        activeTab.classList.add('active');

        allTabContent.forEach(content => {
            content.classList.remove('active');
        });
        let activeContent = document.getElementById(e.dataset.target.slice(1,e.dataset.target.length));
        activeContent.classList.add('active');
    })
});

// Photos to add gallery
const gallleryImages = [
    {
        title: '#GraphicDesign',
        url: 'gallery-images/graphicDesign/graphic-design1.jpg'
    },
    {
        title: '#GraphicDesign',
        url: 'gallery-images/graphicDesign/graphic-design2.jpg'
    },
    {
        title: '#GraphicDesign',
        url: 'gallery-images/graphicDesign/graphic-design3.jpg'
    },
    {
        title: '#GraphicDesign',
        url: 'gallery-images/graphicDesign/graphic-design4.jpg'
    },
    {
        title: '#GraphicDesign',
        url: 'gallery-images/graphicDesign/graphic-design5.jpg'
    },
    {
        title: '#GraphicDesign',
        url: 'gallery-images/graphicDesign/graphic-design6.jpg'
    },
    {
        title: '#GraphicDesign',
        url: 'gallery-images/graphicDesign/graphic-design7.jpg'
    },
    {
        title: '#GraphicDesign',
        url: 'gallery-images/graphicDesign/graphic-design8.jpg'
    },
    {
        title: '#GraphicDesign',
        url: 'gallery-images/graphicDesign/graphic-design9.jpg'
    },
    {
        title: '#GraphicDesign',
        url: 'gallery-images/graphicDesign/graphic-design10.jpg'
    },
    {
        title: '#GraphicDesign',
        url: 'gallery-images/graphicDesign/graphic-design11.jpg'
    },
    {
        title: '#GraphicDesign',
        url: 'gallery-images/graphicDesign/graphic-design12.jpg'
    },
    {
        title: '#LandingPages',
        url: 'gallery-images/landingPage/landing-page1.jpg'
    },
    {
        title: '#LandingPages',
        url: 'gallery-images/landingPage/landing-page2.jpg'
    },
    {
        title: '#LandingPages',
        url: 'gallery-images/landingPage/landing-page3.jpg'
    },
    {
        title: '#LandingPages',
        url: 'gallery-images/landingPage/landing-page4.jpg'
    },
    {
        title: '#LandingPages',
        url: 'gallery-images/landingPage/landing-page5.jpg'
    },
    {
        title: '#LandingPages',
        url: 'gallery-images/landingPage/landing-page6.jpg'
    },
    {
        title: '#LandingPages',
        url: 'gallery-images/landingPage/landing-page7.jpg'
    },
    {
        title: '#WebDesign',
        url: 'gallery-images/webDesign/web-design1.jpg'
    },
    {
        title: '#WebDesign',
        url: 'gallery-images/webDesign/web-design2.jpg'
    },
    {
        title: '#WebDesign',
        url: 'gallery-images/webDesign/web-design3.jpg'
    },
    {
        title: '#WebDesign',
        url: 'gallery-images/webDesign/web-design4.jpg'
    },
    {
        title: '#WebDesign',
        url: 'gallery-images/webDesign/web-design5.jpg'
    },
    {
        title: '#WebDesign',
        url: 'gallery-images/webDesign/web-design6.jpg'
    },
    {
        title: '#WebDesign',
        url: 'gallery-images/webDesign/web-design7.jpg'
    },
    {
        title: '#Wordpress',
        url: 'gallery-images/wordPress/wordpress1.jpg'
    },
    {
        title: '#Wordpress',
        url: 'gallery-images/wordPress/wordpress2.jpg'
    },
    {
        title: '#Wordpress',
        url: 'gallery-images/wordPress/wordpress3.jpg'
    },
    {
        title: '#Wordpress',
        url: 'gallery-images/wordPress/wordpress4.jpg'
    },
    {
        title: '#Wordpress',
        url: 'gallery-images/wordPress/wordpress5.jpg'
    },
    {
        title: '#Wordpress',
        url: 'gallery-images/wordPress/wordpress6.jpg'
    },
    {
        title: '#Wordpress',
        url: 'gallery-images/wordPress/wordpress7.jpg'
    },
    {
        title: '#Wordpress',
        url: 'gallery-images/wordPress/wordpress8.jpg'
    },
    {
        title: '#Wordpress',
        url: 'gallery-images/wordPress/wordpress9.jpg'
    },
    {
        title: '#Wordpress',
        url: 'gallery-images/wordPress/wordpress10.jpg'
    }
]

//Gallery Tabs
let galleryTabs = document.querySelectorAll("[data-filter]");

galleryTabs.forEach(e => {
    e.addEventListener('click',() => {
        galleryTabs.forEach(el => {
            el.classList.remove('active');
        });

        e.classList.add('active');
    })
});

// Adding photos to DOM
const galleryWrapper = document.getElementById('gallery-wrapper');
galleryTabs.forEach( tab => {
    function addAll() {
        if(tab.classList.contains('active') && tab.id === '#All') {
            for( let i = 0; i < 12; i++) {
                const allImage = document.createElement('img');
                allImage.src = gallleryImages[i].url;
                allImage.classList.add('gallery-image');
                galleryWrapper.appendChild(allImage);
            }
        }
    }
    addAll();
    tab.addEventListener('click', () => {
        document.querySelectorAll('.gallery-image').forEach((e) => {
            e.remove();
        })
        for( let i = 0; i < gallleryImages.length; i++) {
            const image = document.createElement('img');
            image.src = gallleryImages[i].url;
            image.classList.add('gallery-image');
            if(tab.classList.contains('active') && gallleryImages[i].title === tab.id) {
                galleryWrapper.appendChild(image);
            }
        }
        addAll();
        hoverEffect();
    } )
})

// Load more button
const btn = document.querySelector('.load-more');
btn.addEventListener('click', (e) => {
    const rest = gallleryImages.slice(12, 24);
    rest.forEach((item) => {
        const moreImg = document.createElement('img');
        moreImg.src = item.url;
        moreImg.classList.add('gallery-image');
        galleryWrapper.appendChild(moreImg);
        btn.remove();
    })
    hoverEffect();
})


//Hover effect
function hoverEffect() {
    const galleryPhotos = document.querySelectorAll('.gallery-image');
    galleryPhotos.forEach(photo => {
    let oldPhoto = photo;
    let hoverDiv = document.createElement('div');
    hoverDiv.classList.add('hover-div');

    //Icons
    let hoverIcon = document.createElement('img');
    hoverIcon.classList.add('hover-icon');
    hoverIcon.src = "img/hover_icons.png";
    hoverDiv.appendChild(hoverIcon);

    //Creative desing
    let constText = document.createElement('span');
    constText.innerText = "CREATIVE DESIGN"
    constText.classList.add('const-text');
    hoverDiv.appendChild(constText);

    // Title text
    let titleText = document.createElement('span');
    titleText.classList.add('title-text');
    let myTitle = gallleryImages[Math.floor(Math.random()*gallleryImages.length)]
    titleText.innerHTML = myTitle.title.slice(1,myTitle.length);
    hoverDiv.appendChild(titleText);

    //Listeneers
    photo.addEventListener('mouseenter', () => {
        photo.parentNode.replaceChild(hoverDiv, photo);
        console.log(oldPhoto);
    })

    hoverDiv.addEventListener('mouseleave', () => {
        hoverDiv.parentNode.replaceChild(oldPhoto, hoverDiv);
        console.log(oldPhoto);
    })
})
}

hoverEffect();


